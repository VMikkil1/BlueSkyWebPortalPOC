import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {SubscribersModule} from './subscribers/subscribers.module';
import {ReportsModule} from './reports/reports.module';
import {FinanceModule} from './finance/finance.module';
import {PartnersModule} from './partners/partners.module';
import {UsersModule} from './users/users.module';
import { FormsModule }  from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import {FuseComponentsModule } from '@tmobile/fuse-components';



@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    DashboardModule,
    SubscribersModule,
    ReportsModule,
    FinanceModule,
    PartnersModule,
    UsersModule,
    StoreModule.forRoot({}, {}),
    FuseComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
