import {Injectable, OnInit} from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import {BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, retry, take } from 'rxjs/operators';
import * as properties from '../../../config.json';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  constructor(private http: HttpClient) {
    debugger;
    this.getBearerToken();

  }
  private token$: BehaviorSubject<string> = new BehaviorSubject("");


  private handleError(error: HttpErrorResponse):Observable<any> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // Return an observable with a user-facing error message.
    return new Observable;
  }
   getBearerToken(): string {
    const httpHeaders = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/xml',
        'Authorization': properties.authHeader
      })

    }
    let tkn: string = ""
    const response =
       this.http.post(properties.authUrl, undefined, httpHeaders)
        .pipe(
          catchError(this.handleError)
        ).pipe(take(1)).subscribe(res =>{
         tkn = res.id_token;

         if(!this.token$){
            this.token$ = new BehaviorSubject(tkn);
          }else{
            this.token$.next(tkn);
          }
       })

    return this.token$.getValue();
  }
  searchSim(iccid: string): Observable<Object>{
    let response: Observable<Object> = new Observable;
    this.getBearerToken();
    const tkn = this.token$.getValue();
    const httpHeaders = {
      headers: new HttpHeaders({
        'sender-id':  properties["sender-id"],
        'partner-id':  properties["partner-id"],
        'Authorization':`Bearer ${tkn}`,
        'partner-transaction-id' : properties["partner-transaction-id"],
        'Content-Type':  properties["Content-Type"],
      })
    }
    const body = {
      "iccid": iccid
    }
    this.http.post('', body, httpHeaders)
      .pipe(catchError(this.handleError))
      .pipe(take(1))
      .subscribe(res => response = res );
    return response;
  }
}
