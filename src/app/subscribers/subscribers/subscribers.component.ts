import {Component, Input, OnInit} from '@angular/core';
import { ConfigService } from 'src/app/config/config.service';

@Component({
  selector: 'app-subscribers',
  templateUrl: './subscribers.component.html',
  styleUrls: ['./subscribers.component.css']
})
export class SubscribersComponent implements OnInit {

  public simInput ='';
  constructor(private configService: ConfigService ) { }

  ngOnInit(): void {
  }
  searchSim(){
     //this.configService.searchSim("8901260963145643651");
    this.configService.searchSim(this.simInput);
  }
}
