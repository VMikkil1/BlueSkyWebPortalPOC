import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubscribersComponent } from './subscribers/subscribers.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FuseComponentsModule } from '@tmobile/fuse-components';



@NgModule({
  declarations: [SubscribersComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FuseComponentsModule
  ],
  exports:[
    SubscribersComponent
  ]

})
export class SubscribersModule { }
